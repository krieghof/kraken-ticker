/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

const St = imports.gi.St;
const Main = imports.ui.main;
const Mainloop = imports.mainloop;
const Lang = imports.lang;

// HTTP
const Soup = imports.gi.Soup;

class Extension {
    constructor() {
        this.panelButton = new St.Bin({
            style_class: "panel-button",
            x_align: St.Align.MIDDLE,
            y_align: St.Align.END
        });

        this.panelButtonText = new St.Label({
            style_class: "price-text",
            text: "Connecting"
        });

        this.panelButton.set_child(this.panelButtonText);

        // Create HTTP Request
        this.msg = Soup.Message.new('GET', 'https://api.kraken.com/0/public/Ticker?pair=XBTUSD');
        this.sessionAsync = new Soup.SessionAsync();
    }

    updatePrice() { // TODO: Reset status_code, the status code is always the same when it was once 200
        let styleClassName;
        let responseText;

        // Send HTTP Request
        this.sessionAsync.send_message(this.msg);

        if (this.msg.status_code === 200) { // Okay

            const parsed = JSON.parse(this.msg.response_body.data);
            let last_close = parsed.result.XXBTZUSD.c[0];
            let today_open = parsed.result.XXBTZUSD.o;

            last_close = parseInt(last_close);
            today_open = parseInt(today_open);

            let percentage = ((last_close - today_open) / today_open * 100).toFixed(2);

            styleClassName = (last_close > today_open) ? "positive" : "negative";
            responseText = percentage.toString() + "%" + " | " + last_close.toString() + "$";
        } else { // Not Okay
            styleClassName = "neutral";
            responseText = "Connection Error"
        }

        this.panelButtonText.set_style_class_name(styleClassName);
        this.panelButtonText.set_text(responseText);
        return true;
    }

    enable() {
        this.timeout = Mainloop.timeout_add_seconds(5.0, Lang.bind(this, this.updatePrice));
        Main.panel._rightBox.insert_child_at_index(this.panelButton, 1);
    }

    disable() {
        Mainloop.source_remove(this.timeout);
        Main.panel._rightBox.remove_child(this.panelButton);
    }
}

function init() {
    return new Extension();
}

